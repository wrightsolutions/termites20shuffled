# Cons2.TERMITES20X hex input - shuffled and processed

## Shuffle process used

The script multiput0x.py exists in the following repositories and uses a onetime key of `2112312020190909`

+[sal683onetime20](https://bitbucket.org/wrightsolutions/sal683onetime20)

+[sal683onetime19](https://bitbucket.org/wrightsolutions/sal683onetime19)  ... older and less preferred version


## Input hashes plus Rowcounts and why not a round 100000 or 200000 in every case?

Each shuffled version of the hex of TERMITES20X is pre-validated to ensure entropy (usually passes) and
decimal equivalent is sufficiently large

Failures have been removed, reducing the round 100000 input to a bit less.

Directory rows100000/ contains the shuffled hex we will use as input to the hashing process


## Source as csv

Because the csv source takes up a bit more space than the results output I have not provided it in every case so
as not to bloat the repo too much

Directory rowsource/ has the csv data for some of the hashing runs.


## Output samples

Directory ver20multiput0x/ contains the results of the hashing process


